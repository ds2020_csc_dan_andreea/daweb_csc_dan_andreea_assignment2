package com.andreea;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Sender {

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();

        try(Connection connection= factory.newConnection()){
            Channel channel = connection.createChannel();

            ///this gives me a queue
            channel.queueDeclare("medical-alert", false, false, false, null);
            try{
                File myObj = new File("activity.txt");
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    String[] splitData={};
                    splitData = data.split("\\s{2,10}");
                    Activity activity = new Activity(splitData[0], splitData[1], splitData[2]);

                    Gson gson = new Gson();
                    String message = gson.toJson(activity);

                    channel.basicPublish("", "medical-alert", false, null, message.getBytes());
                    System.out.println("MEDICAL ALERT for pacient "+activity.getPacient_id()+" has been sent!");
                }
                myReader.close();
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }
}
