package com.andreea;

import java.util.UUID;

public class Activity {

    private UUID pacient_id;
    private String start;
    private String end;
    private String activity;

    public Activity(String start, String end, String activity){
        pacient_id = UUID.randomUUID();
        this.start=start;
        this.end=end;
        this.activity=activity;
    }

    public UUID getPacient_id() {
        return pacient_id;
    }

    public void setPacient_id(UUID pacient_id) {
        this.pacient_id = pacient_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
