package com.andreea;

import com.google.gson.Gson;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeoutException;

public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        ///this gives me a queue
        channel.queueDeclare("medical-alert", false, false, false, null);
        channel.basicConsume("medical-alert", true, (s, delivery) -> {
            String line = new String(delivery.getBody(),"UTF-8");
            Gson gson = new Gson();
            Activity activity = gson.fromJson(line, Activity.class);

            String start = activity.getStart();
            String end = activity.getEnd();
            String activity_name = activity.getActivity();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startTime = LocalDateTime.parse(start, formatter);
            LocalDateTime endTime = LocalDateTime.parse(end, formatter);
            long diff = ChronoUnit.SECONDS.between(startTime, endTime);
            int firstRule = 7;
            int secondRule = 5;
            int thirdRule = 30;

            System.out.println("Pacient's DATA: "+activity.getPacient_id()+"  "+activity.getStart()+"  "+activity.getEnd()+"  "+activity.getActivity());
            if(activity_name.equals("Sleeping")){
                if(diff>firstRule*3600){
                    System.out.println("ALERT! Sleeping too much!\n");
                }
            }else if(activity_name.equals("Leaving")){
                if(diff>secondRule*3600){
                    System.out.println("ALERT! Outside for too long!\n");
                }
            }else if(activity_name.equals("Toileting") || activity_name.equals("Showering")){
                if(diff>thirdRule*60){
                    System.out.println("ALERT! In the bathroom for too long!\n");
                }
            }

        }, s -> {});
    }
}
